host := `uname -a`

##############
# Full Stack #
##############

start-all:
  just watch-dev &
  just yarn-dev &
  just mongo-start 
  just redis-start

stop-all:
  kill -9 $(ps -e | grep cargo-watch | awk '{print $1;}')
  kill -9 $(ps -e | grep yarn | awk '{print $1;}') 
  just mongo-stop 
  just mongo-clean

##########
# Engine #
##########

watch-dev:
  cd engine && RUST_LOG=debug cargo watch -x run

check:
  cd engine && cargo clippy --fix

test:
  cd engine && cargo test

engine-pack:
  podman build -t storyteller:engine -f Dockerfile.engine

############
# Mongo DB #
############

mongo-start: 
  mkdir -p database && \
  podman run -d -p 27017:27017 -v $(pwd)/database:/data/db:Z --name storyteller-mongo \
    -e MONGO_INITDB_ROOT_USERNAME=admin \
    -e MONGO_INITDB_ROOT_PASSWORD=admin123 \
    mongo

mongo-stop:
  podman rm $(podman ps | grep mongo | awk '{print $1;}') --force
  
mongo-clean:
  sudo rm database/* -rf

mongo-connect:
  podman exec -it $(podman ps | grep mongo | awk '{print $1;}') mongo -u admin -p admin123 --authenticationDatabase admin

#########
# Redis #
#########

redis-start:
  podman help

############
# Frontend #
############

yarn-dev:
  cd chronicler && yarn dev
