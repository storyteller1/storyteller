import type { NextPage } from 'next'
import styles from '../styles/Home.module.css'

const Storyteller: NextPage = () => {
  return (
    <main className={styles.main}> 
      <h1 className={styles.title}> 
        Welcome to the world, Storyteller.
      </h1> 
  
      <p className={styles.description}>
        Guide the shared imagined space and spin a tale.
      </p>

      <a href="/" className={styles.card}> 
        <h2>Return Home &#8617;</h2>
      </a>
    </main>
  )
}

export default Storyteller

