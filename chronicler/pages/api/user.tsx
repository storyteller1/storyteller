// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import getConfig from 'next/config'
const { publicRuntimeConfig } = getConfig()

type Data = {
  username: string
}

// Fetch status from the Engine healthcheck endpoint
export default async function handler_two(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
    const name = req.body.name;
    const addr = publicRuntimeConfig.ENGINE_ADDR + publicRuntimeConfig.ROUTES.userAdd;
    const resp = await fetch(
      addr,
      {
        body: JSON.stringify({
          name: name
        }),
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'POST'
      }
    );

    //console.log(resp)
    const data = await resp.json();

    res.status(200).json({ username: data.username })
}

