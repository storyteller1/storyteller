// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'

type Data = {
  engine_status: string
}

// Fetch status from the Engine healthcheck endpoint
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  if (req.method === 'GET') {
    try {
      const resp = await fetch(`http://localhost:8000/health`);
      const data = await resp.json();

      if (data.status === "UP") {
        res.status(200).json({ engine_status: data })
      } else {
        res.status(200).json({ engine_status: "DOWN" })
      }
    } catch (e) {
        res.status(200).json({ engine_status: "DOWN" })
    }
  }
}

