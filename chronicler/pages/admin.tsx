import type { NextPage } from 'next'
import styles from '../styles/Home.module.css'

// TODO: Reconsider using <a> instead of <button>
const About: NextPage = () => {
  return (
    <main className={styles.main}> 
      <h1 className={styles.title}> 
        Administrator Control
      </h1> 
  
      <p className={styles.description}>
        Control any aspect of the system from here.
      </p>

      <a href="/admin" className={styles.card}> 
        <h2>Manage Players &rarr;</h2>
      </a>

      <a href="/admin" className={styles.card}> 
        <h2>View Worlds &rarr;</h2>
      </a>

      <a href="/admin" className={styles.card}> 
        <h2>Set Active World &rarr;</h2>
      </a>

      <a href="/" className={styles.card}> 
        <h2>Return Home &#8617;</h2>
      </a>
    </main>
  )
}

export default About

