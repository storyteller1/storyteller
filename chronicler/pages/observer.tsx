import type { NextPage } from 'next'
import styles from '../styles/Home.module.css'

const Observer: NextPage = () => {
  return (
    <main className={styles.main}> 
      <h1 className={styles.title}> 
        Welcome to the world, Observer.
      </h1> 
  
      <p className={styles.description}>
        Observe the shared imagined space and enjoy the story.
      </p>

      <a href="/" className={styles.card}> 
        <h2>Return Home &#8617;</h2>
      </a>
    </main>
  )
}

export default Observer

