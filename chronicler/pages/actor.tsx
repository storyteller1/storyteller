import type { NextPage } from 'next'
import styles from '../styles/Home.module.css'
import React from 'react'


const Actor: NextPage = () => {

  const [showLogin, setShowLogin] = React.useState(false);
  const displayLogin = () => {
    setShowLogin(val => !val)
  }

  return (
    <main className={styles.main}> 

      { !showLogin && <Login /> }
      { showLogin && <Journal /> }

      { !showLogin &&
        <button onClick={displayLogin} className={styles.card}> 
          <h2>Tell Your Story &rarr;</h2>
        </button>
      }

      <a href="/" className={styles.card}> 
        <h2>Return Home &#8617;</h2>
      </a>
    </main>
  )
}

// TODO: Setup component state to SHOW/HIDE this element when 
//       the "Tell Your Story" button is pressed.
const Journal = () => {

    const registerUser = async event => {
      event.preventDefault();

      try {
        const res = await fetch(
          '/api/user',
          {
            body: JSON.stringify({
              name: event.target.name.value    
            }),
            headers: {
              'Content-Type': 'application/json'
            },
            method: 'POST'
          }
        );

        const result = await res.json();
        console.log(result);
      } catch (e) {
        console.log(e)
      }
    }

    return (
      <main className={styles.main}>
        <h1 className={styles.title}>
          My name is ... 
        </h1>

        <form onSubmit={registerUser}>
          <input id="name" name="name" type="text" className={styles.centered_element} /> 
        </form>
      </main>
    )
}

const Login = () => {
  return (
    <div>
      <h1 className={styles.title}> 
        Welcome to the world, Actor.
      </h1> 
  
      <p className={styles.description}>
        Peer into the shared imagined space, and create a legacy for yourself.
      </p>
    </div>
  )
}

export default Actor

