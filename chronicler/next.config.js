/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  publicRuntimeConfig: {
    ENGINE_ADDR: 'http://localhost:8000',
    ROUTES: {
      userAdd: '/user'
    }
  }
}

module.exports = nextConfig
