# Storyteller
A play-by-post system for table top roleplaying games brought to life in text.

## Components
### Backend 
#### Engine
The backend system facilitating the serving of data to the frontend. Written in Rust using Axum.

#### Database
MongoDB was picked as the database to store world, player, and chat data.

### Frontend 
#### Chronicler
A NextJS application the players, storytellers, and administrators can interact with to manage their game.

#### Cache Layer
I'm planning to implement REDIS as a cache layer to help with searching chat log data, etc.

## Development
I've opted to use a Justfile to manage my development tasks. From the root folder, run a "just" command ( eg. `just watch-dev` ) to manage the project. You may read more about `just` here: https://github.com/casey/just.
