use mongodb::{Client, Database, options::ClientOptions, results::InsertOneResult};
use serde::Serialize;

use std::error::Error;

mod collections;

pub async fn connect_db() -> Result<Database, Box<dyn Error>> {

    // TODO: abstract away the username and password in the connection string 
    // TODO: add a non-admin user for the engine to connect with 
    // Parse a connection string into an options struct.
    let mut client_options = ClientOptions::parse("mongodb://admin:admin123@localhost:27017").await?;

    // Manually set an option.
    client_options.app_name = Some("engine".to_string());

    // Get a handle to the deployment.
    let client = Client::with_options(client_options)?;

    // List the names of the databases in that deployment.
    for db_name in client.list_database_names(None, None).await? {
        tracing::debug!("Found available databases: {}", db_name);
    }

    // Get a database handle 
    let db = client.database("test");
    tracing::info!("Attempting to establish database connection.");

    Ok(db)
}

// TODO: add code to initialize a USERS collection if none exists. 
pub async fn init_db(
    db: Database
    ) -> Result<(), Box<dyn Error>> {
    
    let collection = "users";
    let user = collections::User {
        username: "Jessica Orrett".to_string(),
        id: 001,
    };

    let insert_generic = insert_one(db, collection, user).await;
    tracing::info!("{:#?}", insert_generic.unwrap());

    Ok(())
}

// This is treated as "DEAD CODE" because there is a generic implementation
// to perform an "insert_one" below which is not locked to a specific struct
async fn _create_user(
    db: Database, 
    name: String
    ) -> Result<InsertOneResult, Box<dyn Error>> {
    let typed_collection = db.collection::<collections::User>("users");
    let user = collections::User {
        username: name,
        id: 000
    };

    let insert_results = typed_collection.insert_one(user, None).await?;

    Ok(insert_results)
}

// Insert a generic struct which implements "Serialize" as a single document
async fn insert_one<T: Serialize>(
    db: Database, 
    collection: &str, 
    document: T
    ) -> Result<InsertOneResult, Box<dyn Error>> {
    let typed_collection = db.collection(collection);
    let insert_result = typed_collection.insert_one(document, None).await?;

    Ok(insert_result)
}
