use axum::{
    extract::{
        ws::WebSocketUpgrade,
        Extension,
    },
    response::{Html, IntoResponse},
    http::StatusCode,
    Json,
};

use chrono::{DateTime, Utc};

use std::sync::Arc;

mod chat;
pub mod data;

// Return 200, Json Object("UP", TIMESTAMP)
// The return signature of this function "impl IntoResponse"
// means to say that "the type returned from this function
// will implement this trait".
pub async fn health() -> impl IntoResponse {
    let now: DateTime<Utc> = Utc::now();

    let status = data::Health {
        status: "UP".to_owned(),
        timestamp: now.to_string()
    };
    
    (StatusCode::OK, Json(status))
}

// Create user and ID 
pub async fn user(
    Json(payload): Json<data::CreateUser>,
) -> impl IntoResponse {
   
    tracing::info!("{:#?}", payload);

    let user = data::User {
        username: payload.name,
    };

    (StatusCode::CREATED, Json(user))
}

pub async fn websocket_handler(
    ws: WebSocketUpgrade,
    Extension(state): Extension<Arc<data::AppState>>,
) -> impl IntoResponse {
    ws.on_upgrade(|socket| chat::websocket(socket, state))
}

// Include utf-8 file at **compile** time.
pub async fn index() -> Html<&'static str> {
    Html(std::include_str!("../chat.html"))
}

