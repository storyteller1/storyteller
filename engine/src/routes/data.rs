use serde::{Deserialize, Serialize};
use std::{
    collections::HashSet,
    sync::Mutex
};

use tokio::sync::broadcast;

/*
 * Incoming Payloads 
 */

// The input to our `user` handler
// referring to the incoming payload
// from the front-end server
#[derive(Deserialize, Debug)]
pub struct CreateUser {
    pub name: String,
}


/*
 * Internal Data Structures
 */

// the output to our `create_user` handler
#[derive(Serialize)]
pub struct User {
    pub username: String,
}

#[derive(Serialize)]
pub struct Health {
    pub status: String,
    pub timestamp: String,
}

// Our shared state
pub struct AppState {
    pub user_set: Mutex<HashSet<String>>,
    pub tx: broadcast::Sender<String>,
}
