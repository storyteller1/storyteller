use axum::{
    routing::{get, post},
    Router,
    extract::Extension,
};
use tokio::sync::broadcast;
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};
use axum::http::Method;
use tower_http::cors::{CorsLayer, Any};

use std::{
    collections::HashSet,
    net::SocketAddr,
    sync::{Arc, Mutex},
};

mod mongo;
mod routes;

// Set server bind address & port
const ADDR: [u8; 4] = [0, 0, 0, 0];
const PORT: u16 = 8000;

// RUNTIME
#[tokio::main] 
async fn main() {
    // Initialize logging
    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::new(
            std::env::var("RUST_LOG").unwrap_or_else(|_| "storytelling_engine=trace".into()),
        ))
        .with(tracing_subscriber::fmt::layer())
        .init();

    // Start Database Connection
    let _conn = mongo::connect_db().await;
    let _test = mongo::init_db(_conn.unwrap()).await;

    // Setup CORS
    let cors = CorsLayer::new()
        .allow_methods(vec![Method::GET, Method::POST])
        .allow_origin(Any);

    // Initialize application state, router
    let user_set = Mutex::new(HashSet::new());
    let (tx, _rx) = broadcast::channel(100);
    let app_state = Arc::new(routes::data::AppState { user_set, tx });
    let app = Router::new()
        .route("/", get(routes::index))
        .route("/websocket", get(routes::websocket_handler))
        .route("/health", get(routes::health))
        .route("/user", post(routes::user))
        .layer(Extension(app_state))
        .layer(cors);

    // Start server
    let addr = SocketAddr::from((ADDR, PORT));
    tracing::info!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

