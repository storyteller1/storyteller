#-------------------#
# Cargo Build Stage #
#-------------------#
FROM rustlang/rust:nightly as builder
WORKDIR /usr/src/engine

# Build Deps, Cache & Clean Up
COPY engine/Cargo.toml Cargo.toml
RUN mkdir src/ && echo "fn main() {println!(\"if you see this, the build broke\")}" > src/main.rs
RUN cargo build --release
RUN rm -f target/release/deps/engine*

# Build Application
COPY engine/ .
RUN cargo build --release
RUN cargo install --path .

#---------------------#
# Service Build Stage #
#---------------------#
FROM debian:stable-slim

# Add Underprivileged User
RUN addgroup -uid 1000 engine && \
  useradd -s /bin/bash -g engine -G engine -u 1000 engine

RUN mkdir -p /usr/share/man/man1 /usr/share/man/man7 && \
  apt-get update && \
  apt-get upgrade -y

COPY --from=builder /usr/src/engine /usr/local/bin/engine

# Run Engine
WORKDIR /usr/local/bin/
RUN chown -R engine:engine engine
RUN chmod +x engine/target/release/engine
USER engine
CMD ./engine/target/release/engine
